#ifndef _INC
#include "inc.h"
#endif

int p_callpass(int(*f)(FILE *), FILE *s, int debugp)
{
  if(debugp) {
    printf("Calling the function\n");
    printf("Function returned %d\n", (*f)(s));
  }
  else return (*f)(s);
}

int p_braceeq(FILE *s)
{
  char c; int i = 0;
  while((c = getc(s)) != EOF && i>-1) {
    if(!c) break;
    if(c == ')') i--;
    if(c == '(') i++;
  }
  if(!c && !feof(s)) perror("p_braceeq");
  if(i != 0) e_fail(e_pbraceeq);
  rewind(s);
  return i;
}

/* int p_splbrace(FILE *s) */
/* { */
/*   char c; */
/*   while((c = fgetc(s)) != EOF) { */
/*     if(!c) break; */
/*     if(c == '(') { */
/*       s_insprior(s, ' '); */
/*       s_inspost(s, ' '); */
/*     } */
/*   } */
/*   if(!c && !feof(s)) e_fail(e_psplbrace); */
/*   rewind(s); */
/*   return 0; */
/* } */
