#define _EMSG

#define e_fnop      "File descriptor does not exist!"

#define e_lfnop     "lfile does not exist!"
#define e_lcreat    "Unable to allocate new link_t."
#define e_lofnex    "Specified offset is greater than lfile length."

#define e_pbraceeq  "Trailing brace found, or braces not matched."
#define e_psplbrace "."
