#ifndef _INC
#include "inc.h"
#endif

/* Generally, s denotes a string, however here it denotes a stream */

int main(int argc, char **argv)
{
  LFILE *s;
  if(argc>0) s = l_fopen(argv[1]);
  /* else s = stdin; */

  /* char *buf1 = (s->l)->s; */

  char *buf = "Helloworld";

  printf("Before: \n");
  l_fwrite(stdout, s);
  printf("--END--\n");

  l_write(buf, strlen(buf), 21, s);

  printf("\nAfter: \n");
  l_fwrite(stdout, s);
  printf("--END--\n");

  l_fclose(s);

  /* p_callpass((int (*)(LFILE *))p_braceeq, s, 0); */
  /* callpass((int (*)(LFILE *))p_splbrace, s, 1); */
  /* Stream printing function here */
  return 0;
}
