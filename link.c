#ifndef _INC
#include "inc.h"
#endif

/* Creates a new link_t node. If a previous node is supplied, then the
 * new node is inserted between the previous node and the node
 * following the previous node.
 */
static struct link_t *l_creat(struct link_t *p)
{
  struct link_t *tp;
  tp = malloc(sizeof(struct link_t));
  if(!tp) e_fail(e_lcreat);

  *(tp->s) = '\0';
  tp->p = NULL;
  tp->n = NULL;

  if(!p) return tp;
  if(!p->n) {
    p->n = tp;
    tp->p = p;
  }
  else {
    tp->n = p->n;
    (p->n)->p = tp;

    p->n = tp;
    tp->p = p;
  }
  return tp;
}

/* Removes the link_t node from the link_t list and frees the node */
static void l_destr(struct link_t *n)
{
  if(n->p) (n->p)->n = n->n;
  if(n->n) (n->n)->p = n->p;
  free(n);
}

/* Returns an LFILE system with enough space to contain the file LFILE */
static LFILE *l_falloc(FILE *fd)
{
  if(!fd) e_fail(e_fnop);
  fseek(fd, 0L, SEEK_END);
  long len = ftell(fd);
  rewind(fd);
  int nl = len / l_BUFSIZE;
  if(len%l_BUFSIZE > 0) nl++;

  struct link_t *root = l_creat(NULL);
  struct link_t *n = root;
  while(nl-->0) {
    l_creat(n);
    n = n->n;
  }
  LFILE *r = malloc(sizeof(LFILE));
  r->l = root;
  r->pos = 0;
  return r; /* If this doesn't work then we'll have to sudo ;) */
}

/* Allocates enough space for the file with the name 'filename', then
 * reads in the content from that FILE into the LFILE.
 * Acts as a drop-in replacement to fopen().
 */
LFILE *l_fopen(const char *filename)
{
  FILE *fd = fopen(filename, "r");
  if(!fd) perror(filename);
  LFILE *root = l_falloc(fd);
  if(!root) e_fail(e_lfnop);
  l_fread(fd, root);
  fclose(fd);
  return root;
}

/* Frees the memory occupied by the LFILE f and then frees the LFILE f
 * Acts as a drop-in replacement to fclose()
 */
void l_fclose(LFILE *f)
{
  if(!f) e_fail(e_lfnop);
  struct link_t *n = f->l;
  f->l = NULL;

  while(n->n) {
    n = n->n;
    l_destr(n->p);
  }

  if(n) l_destr(n);
  free(f);
}

/* l_fread reads in the entirety of the LFILE f to the FILE fd
 * The callee must ensure there are enough links to read the entire file
 */
size_t l_fread(FILE *fd, LFILE *f)
{
  if(!f) e_fail(e_lfnop);
  struct link_t *n = f->l;
  if(!fd) e_fail(e_fnop);
  size_t i = 0;
  while(!feof(fd) && !ferror(fd) && n->n) {
    i += fread(n->s, l_BUFSIZE+1, 1, fd);
    n = n->n;
  }
  if(ferror(fd)) perror("l_fread");
  return i;
}

/* l_fwrite copies the entire contents of the LFILE f to the FILE fd */
size_t l_fwrite(FILE *fd, LFILE *f)
{
  if(!f) e_fail(e_lfnop);
  if(!fd) e_fail(e_fnop);

  struct link_t *n = f->l;
  size_t i = 0;
  while(!feof(fd) && !ferror(fd) && n->n) {
    i += fwrite(n->s, l_BUFSIZE+1, 1, fd);
    n = n->n;
  }
  fflush(fd);
  if(ferror(fd)) perror("l_fwrite");
  return i;
}

/* l_read reads chars from the LFILE f to s, beginning at
 * f.pos + offset and ending at ((f.pos + offset)+size)-1.
 * A drop in replacement for fread().
 */
size_t l_read(char *s, size_t size, size_t offset, LFILE *f)
{
  if(!f) e_fail(e_lfnop);
  struct link_t *n = f->l;

  size_t i = f->pos + offset;
  size_t ln = i/l_BUFSIZE;
  i = i%l_BUFSIZE;

  if(f->pos + offset > 0 && i == 0) {
    ln--;
    i = l_BUFSIZE;
  }

  while(ln-->0 && n->n)
    n = n->n;
  if(ln>0 && !n->n)
    e_fail(e_lofnex);

  char *pt = n->s + i;
  size_t c = 0;
  while(c++ < size) {
    if(!*pt) {
      if(!n->n) break;
      n = n->n;
      pt = n->s;
    }
    *s++ = *pt++;
  }
  return --c;
}

/* l_write copies characters from the buffer s to the LFILE f,
 * beginning at f.pos + offset and ending at ((f.pos + offset)+size)-1.
 * It writes over any text, and will extend the LFILE if there is not
 * enough room to store the full contents of s.
 * A drop in replacement for fwrite() (or close to being one).
 */
size_t l_write(char *s, size_t size, size_t offset, LFILE *f)
{
  if(!f) e_fail(e_lfnop);
  struct link_t *n = f->l;

  size_t i = f->pos + offset;
  size_t ln = i/l_BUFSIZE;
  i = i%l_BUFSIZE;

  if(f->pos + offset > 0 && i == 0) {
    /* Same patch as in l_read */
    ln--;
    i = l_BUFSIZE;
  }

  while(ln-->0 && n->n)
    n = n->n;
  if(ln>0 && !n->n)
    e_fail(e_lofnex);

  printf("\nStarting offset: %d, size: %d\n", i, size);
  char *pt = n->s + i;
  size_t c = size;
  while(c-- > 0) {
    if(!*pt) {
      if(!n->n) l_creat(n);
      n = n->n;
      pt = n->s;
    }
    *pt++ = *s++;
  }
  return size - c;
}

size_t l_trav(LFILE *f, size_t offset, struct link_t **l)
{
  size_t i = f->pos + offset;
  size_t ln = i/l_BUFSIZE;
  i = i%l_BUFSIZE;
  if(f->pos > 0 && i == 0) {
    /* A flimsy plaster that works. Basically stops it from clobbering
     * a character. Later I'll try to understand why this happens,
     * but that is something for later.
     */
    ln--;
    i = l_BUFSIZE;
  }
  while(ln-->0 && (*n)->n)
    *n = (*n)->n;
  if(ln>0 && !(*n)->n)
    e_fail(e_lofnex);

  return i;
}

char l_getc(LFILE *f)
{
  return
}

