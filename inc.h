#define _INC
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifndef _EMSG
#include "emsg.h"
#endif

/* pass.c */
int p_callpass(int(*f)(FILE *), FILE *s, int debugp);
int p_braceeq(FILE *h);

/* link.c */
#define l_BUFSIZE 21
/* 512 */
struct link_t {
  char s[l_BUFSIZE+1]; /* We need to leave room for a NULL char */
  struct link_t *n, *p;
};
typedef struct {
  struct link_t *l;
  size_t pos;
} LFILE;

/* These are for the backend, but they're nice for debugging */
size_t l_fread(FILE *fd, LFILE *n);
size_t l_fwrite(FILE *fd, LFILE *n);

LFILE *l_fopen(const char *filename);
void l_fclose(LFILE *n);
size_t l_read(char *s, size_t size, size_t offset, LFILE *f);
size_t l_write(char *s, size_t size, size_t offset, LFILE *f);

/* err.c */
int e_fail(const char *s);
