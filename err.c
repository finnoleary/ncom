#ifndef _INC
#include "inc.h"
#endif

static int e_useerr = 0;

int e_fail(const char *s)
{
  if(e_useerr)
    fprintf(stderr, "Fatal Error: %s\n", s);
  else
    printf("Fatal Error: %s\n", s);

  exit(1);
}

